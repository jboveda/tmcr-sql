declare @campus table(keyid varchar(3), label varchar(100));
insert into @campus values
('DA' , 'UC Davis'),
('BK' , 'UC Berkeley'),
('GLA', 'GLA'),
('IR' , 'UC Irvine'),
('LA' , 'UCLA'),
('LAL', 'Los Alamos Laboratory'),
('LBL', 'Lawrence Berkeley Lab'),
('LLL', 'Lawrence Livermore Lab'),
('MC' , 'MC'),
('NGL', 'NGL'),
('NUC', 'Other non-university'),
('OP' , 'University of California Office of the President'),
('RV' , 'UC Riverside'),
('SB' , 'UC Santa Barbara'),
('SC' , 'UC Santa Cruz'),
('SD' , 'UC San Diego'),
('SF' , 'UC San Francisco');

select * from @campus;