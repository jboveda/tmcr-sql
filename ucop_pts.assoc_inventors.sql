declare @inventortypes table(keyid varchar(3), label varchar(100));
insert into @inventortypes values
('AGE', 'Agent - HHMI'),
('FAC', 'Faculty'),
('NUC', 'Non-UC Inventor'),
('OTH', 'Other (UC)'),
('PD', 'Post-doctorate'),
('STA', 'Staff'),
('STU', 'Student');

select
--IDs
replace(ltrim(rtrim(inventor.inv_nk)), ' ', '-') + 
ltrim(rtrim('-' + inventor.caseno)) + 
'-' + ltrim(rtrim(inventor.caseseq)) + 
'-' + ltrim(rtrim(cases.cc)) 
as associated_inventor_id,
cases.full_case + '-' + cases.cc as filing_id,

ltrim(rtrim((case 
when inventor.camp != 'DA' then inventor.camp
--when inventor.schl is null and inventor.dept is null then 'DA'
else school.fkey end))) as department_id,
replace(ltrim(rtrim(inventor.inv_nk)), ' ', '-') as inventor_id,
cast(ltrim(rtrim(inventor.invno)) as int) as inventor_sequence,
inventor_type.label as inv_typ,

(case 
when inventor.camp = 'DA' and inventor.invno = 1
-- and inventor.caseseq = '1' -- this may be misleading as it'll mark as non-lead inventors who are actually leads for entire case
then 'UC Inventor (Lead)'
when inventor.camp = 'DA' then 'UC Inventor (Non-lead)'
else 'Non-UC Inventor'
end) as inventor_role,
cases.caseno as sf_id

from
(
	select caseno, caseseq, caseno + '-' + caseseq as full_case, 'US' as cc from UCD_extract.dbo.case_m
	union all
	select caseno, caseseq, caseno + '-' + caseseq as full_case, ctry as cc from UCD_extract.dbo.case_ff
) cases
inner join UCD_extract.dbo.case_inv inventor
on (cases.caseno = inventor.caseno
and cases.caseseq = inventor.caseseq)
left outer join @inventortypes inventor_type
on inventor.inv_typ = inventor_type.keyid
left outer join UCD_extract.dbo.schldept school
on (inventor.camp = school.s_camp
and inventor.dept = school.dept
and inventor.schl = school.schl
)

order by cases.caseno asc

-- as inventor_role,

--name
--ltrim(rtrim(people.prefix)) as salutation,
--ltrim(rtrim(people.fname)) as first_name,
--substring(people.minit, 1, 1) as middle_initial,
--ltrim(rtrim(people.lname)) as last_name,

--organization
--school.fullname as school,
--lower(school.dept) as department,

--contact info
--ltrim(rtrim(ISNULL(people.email_wrk, people.email_hom))) as email,
--ltrim(rtrim(people.m_add0 + ISNULL(people.m_add1, ''))) as street,
--ltrim(rtrim(people.m_add2)) as city,
--ltrim(rtrim(people.m_st)) as state,
--ltrim(rtrim(m_zip)) as zip,
--ltrim(rtrim(m_ctry)) as country,
--ltrim(rtrim(m_tel)) as phone,
--ltrim(rtrim(m_fax)) as fax,

--misc
--ltrim(rtrim(replace(LOWER(people.commnt), '"', ''''))) as addl_information

/*
from 
UCD_extract.dbo.case_inv inventor
inner join UCD_extract.dbo.name_ad people
	on inventor.inv_nk = people.nk
inner join UCD_extract.dbo.schldept school
	on school.schl = inventor.schl
and school.dept = inventor.dept
left join (
	select 
	cse.full_case + '-' + coalesce(ff.ctry, 'US') as filing_id, 
	cse.caseno, 
	cse.caseseq
	from UCD_extract.dbo.case_m cse
	left outer join UCD_extract.dbo.case_ff as ff
	on cse.caseno = ff.caseno and cse.caseseq = ff.caseseq
) cases 
	on inventor.caseno = cases.caseno and inventor.caseseq = cases.caseseq
*/

