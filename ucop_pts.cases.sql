-- load SF users
if object_id('tempdb..#sfusers') is not null drop table #sfusers
go
create table #sfusers 
(
id varchar(18),
federationidentifier varchar(60),
lastname varchar(60),
firstname varchar(60),
is_active varchar(5),
iam_id__c varchar(10),
);
bulk insert #sfusers
from 'D:\Salesforce\csvdrop\sql_queries\salesforce.users.csv'
with (
firstrow = 2,
fieldterminator = ',',
rowterminator = '\n',
formatfile = 'D:\Salesforce\csvdrop\sql_queries\salesforce.users.dataformat.fmt'
);

-- load agreements (required to do complex logic on agreements)
if object_id('tempdb..#agreements') is not null drop table #agreements
go
create table #agreements
(
caseno varchar(8) not null,
agrno varchar(12) not null,
agreement_type int not null,
date_1 date,
date_2 date,
date_3 date,
pros_stat int
);
create index index_caseno ON #agreements (caseno);
insert into #agreements
select d.caseno, m.agrno, cast(substring(m.agrno, 6, 2) as int) agreement_type, 
	case when cast(substring(m.agrno, 6, 2) as int) IN (1, 2, 3, 4, 5, 6, 7, 8, 10, 11, 12, 14, 16, 18, 19) then m.exec_dt else NULL end,
	case when cast(substring(m.agrno, 6, 2) as int) IN (20, 21, 30) then m.exec_dt else NULL end,
	case when cast(substring(m.agrno, 6, 2) as int) IN (21, 23, 24, 25, 26) then m.exec_dt else NULL end,
	c.pros_stat
from UCD_extract.dbo.agr_m m 
	join UCD_extract.dbo.agr_d d on m.agrno = d.agrno
	join UCD_extract.dbo.case_m c on d.caseno = c.caseno
where d.caseseq = '1'

select
/*******************
	case info
*******************/
cases.caseno as uc_case_number,
ltrim(rtrim(LOWER(cases.short_title))) as short_title,
ltrim(rtrim(REPLACE(REPLACE(REPLACE(LOWER(cases.title), '"', ''''), CHAR(13), ''), CHAR(10), ''))) as invention_title,
(case
when bases.pl_ut = 'P' then 'Plant'
when bases.pl_ut = 'U' then 'Utility'
when bases.pl_ut = 'C' then 'Copyright'
when bases.pl_ut = 'H' then 'Hybrid'
else null
end) as case_type,
cast(bases.recd_dt as date) as date_received,
cast(bases.publ_disc_dt as date) as disclosure_date,
licensor.iamid as licensing_officer_iam_id,
licensor.user_name as licensing_officer,
(case cases.pros_stat when 20 then 1 else 0
end) as is_trp,


/*******************
	inventor info
*******************/
replace(ltrim(rtrim(cases.lead_inv)), ' ', '-') as pts_name_id,
ltrim(rtrim((case 
when school.s_camp != 'DA' then school.s_camp
else school.fkey end))) as department_id,

/*******************
	agreement info
*******************/
cast(agreements.agreement_date as date) agreement_date,

/*******************
	monetary info
*******************/
cast(coalesce(receipts.amt, 0) as float) as receipts,
cast(coalesce(expenses.amt, 0) as float) as expenses

from 
-- case base
UCD_extract.dbo.case_base as bases
-- case master file
join UCD_extract.dbo.case_m as cases
on (bases.caseno = cases.caseno)
-- licensing officer
left outer join (
select iam_id__c as iamid, ll.user_name, ll.nk, ll.user_init from #sfusers sf, UCD_extract.dbo.user_auth ll
where lower(ll.user_name) LIKE lower(sf.firstname + '%' + sf.lastname + '%')
) licensor
on (licensor.user_init = bases.lic_init)

-- inventors
join UCD_extract.dbo.case_inv inventor
on (
	inventor.caseno = cases.caseno 
	and inventor.caseseq = cases.caseseq 
	and inventor.invno = '1'
)
-- agreements info
left outer join (
	select m.caseno,
	coalesce(
		MIN(m.date_1), 
		MAX(m.date_2), 
		MIN(m.date_3), 
		null
		)
	--end
	as agreement_date
	from #agreements m
	group by m.caseno
)
agreements on (agreements.caseno = cases.caseno)
-- school/dept info
left outer join UCD_extract.dbo.schldept school
on (
	school.s_camp = inventor.camp
	and school.dept = inventor.dept
	and school.schl = inventor.schl
)

-- receipts
left outer join (
select caseno, sum(amt) as amt from UCD_extract.dbo.receipt 
group by caseno
) receipts
on (bases.caseno = receipts.caseno)
-- expenses
left outer join (
select caseno, sum(amt) as amt from UCD_extract.dbo.exp
where stat = 'P'
and underreview is null
group by caseno
) expenses
on (bases.caseno = expenses.caseno)

where cases.caseseq = '1' 
and inventor.camp = 'DA'

order by cases.full_case desc