set echo off;
spool "/home/jboveda/Documents/Projects/TMCR/csvdrop-prod/tmcr.iam_faculty.fromoracle.csv";

select /*csv*/
--count(*)
people.iamid,
people.d_fname,
people.d_lname,
people.d_fullname,

assc.dept_code,
assc.title_display_name,
assc.percent_fulltime,

contact.email,
contact.addr_street,
contact.addr_city,
contact.addr_state,
contact.addr_zip,
contact.postaladdress,
contact.work_phone,
contact.work_cell,
contact.work_pager,
contact.work_fax

from pr_services.cv_people people
inner join pr_services.ppl_pps_associations assc
on people.iamid = assc.iamid
left outer join pr_services.ppl_contact contact
on people.iamid = contact.iamid

where 
people.IS_FACULTY = 'Y'
order by people.iamid asc, 
assc.percent_fulltime asc, 
assc.assoc_end_date asc;

spool off;