select 
ltrim(rtrim(sponsors.spn)) as sponsor_id,
ltrim(rtrim(lower(sponsors.spn_name))) as sponsor_name,
(case sponsors.spn_typ
when 'GF' then 'Federal'
when 'GS' then 'State'
when 'GC' then 'County'
when 'GL' then 'Local/City'
when 'C'  then 'Commercial'
when 'O'  then 'Other'
when 'N'  then 'Non-profit'
end) as sponsor_type

from 
UCD_extract.dbo.sponsor sponsors

order by sponsor_id asc