set echo off;
spool "/home/jboveda/Documents/Projects/TMCR/csvdrop/tmcr.ais_contacts.fromoracle.csv";

select /*csv*/
people.iamid,
people.d_fname,
people.d_lname,
people.d_fullname,

assc.dept_code,
assc.title_display_name,
assc.percent_fulltime,

contact.email,
contact.addr_street,
contact.addr_city,
contact.addr_state,
contact.addr_zip,
contact.postaladdress,
contact.work_phone,
contact.work_cell,
contact.work_pager,
contact.work_fax

from pr_services.cv_people people
inner join pr_services.ppl_pps_associations assc
on people.iamid = assc.iamid
left outer join pr_services.ppl_contact contact
on people.iamid = contact.iamid

where d_fname in (
'Cheryl',
'Christine',
'Daniel',
'Gregory',
'Hyemi',
'Jacquelyn',
'Janet',
'Joseph',
'Karen',
'Kathy',
'Kelly',
'Lana',
'Leigh',
'Martha',
'Melissa',
'Michael',
'Pamela',
'Paul',
'Robert',
'Sallie',
'Shelley',
'Sumiko',
'Tami',
'Thomas',
'Tony',
'Virginia'
)
and d_lname in (
'August',
'Barrientes',
'Berry',
'Block',
'Charney',
'Cobbs',
'Davis',
'Gibbs',
'Hartman',
'Haworth',
'Hazarian',
'Hong',
'Isidor',
'Kay-Mills',
'Krovoza',
'Mackey',
'Maddex',
'Oppedahl',
'Ozonoff',
'Pacelli',
'Prokop',
'Schmidt',
'Scott',
'Sevening',
'Tate',
'Tentis',
'Venturino',
'Wickliffe'
)
and is_student = 'N'

order by people.iamid asc, 
assc.percent_fulltime asc, 
assc.assoc_end_date asc;

spool off;