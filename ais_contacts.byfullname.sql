set echo off;
spool "/home/jboveda/Documents/Projects/TMCR/csvdrop/tmcr.ais_contacts.fromoracle.csv";

select /*csv*/
people.iamid,
initcap(people.o_fname) as FirstName,
initcap(people.o_lname) as LastName,
d_mname as MiddleInitial,

assc.dept_code,
assc.title_display_name,
assc.percent_fulltime,

contact.email,
contact.addr_street,
contact.addr_city,
contact.addr_state,
contact.addr_zip,
contact.postaladdress,
contact.work_phone,
contact.work_cell,
contact.work_pager,
contact.work_fax

from pr_services.cv_people people
inner join pr_services.ppl_pps_associations assc
on people.iamid = assc.iamid
left outer join pr_services.ppl_contact contact
on people.iamid = contact.iamid

where 
((initcap(o_fname) = 'Alison' or d_fname = 'Alison') and initcap(o_lname) = 'Morr') or 
((initcap(o_fname) = 'Anne' or d_fname = 'Anne') and initcap(o_lname) = 'Bishop') or 
((initcap(o_fname) = 'Bob' or d_fname = 'Bob') and initcap(o_lname) = 'Stout') or 
((initcap(o_fname) = 'Cheryl' or d_fname = 'Cheryl') and initcap(o_lname) = 'Cobbs') or 
((initcap(o_fname) = 'Chong' or d_fname = 'Chong') and initcap(o_lname) = 'Porter') or 
((initcap(o_fname) = 'Christine' or d_fname = 'Christine') and initcap(o_lname) = 'Schmidt') or 
((initcap(o_fname) = 'Colleen' or d_fname = 'Colleen') and initcap(o_lname) = 'Schulman') or 
((initcap(o_fname) = 'Cynthia' or d_fname = 'Cynthia') and initcap(o_lname) = 'Spiro') or 
((initcap(o_fname) = 'Daniel' or d_fname = 'Daniel') and initcap(o_lname) = 'Isidor') or 
((initcap(o_fname) = 'Debbie' or d_fname = 'Debbie') and initcap(o_lname) = 'Wilson') or 
((initcap(o_fname) = 'Elizabeth' or d_fname = 'Elizabeth') and initcap(o_lname) = 'Abad') or 
((initcap(o_fname) = 'Frank' or d_fname = 'Frank') and initcap(o_lname) = 'Teplin') or 
((initcap(o_fname) = 'Gregory' or d_fname = 'Gregory') and initcap(o_lname) = 'Gibbs') or 
((initcap(o_fname) = 'Hyemi' or d_fname = 'Hyemi') and initcap(o_lname) = 'Sevening') or 
((initcap(o_fname) = 'Jacquelyn' or d_fname = 'Jacquelyn') and initcap(o_lname) = 'Kay-Mills') or 
((initcap(o_fname) = 'Janet' or d_fname = 'Janet') and initcap(o_lname) = 'Berry') or 
((initcap(o_fname) = 'Janet' or d_fname = 'Janet') and initcap(o_lname) = 'Krovoza') or 
((initcap(o_fname) = 'Jennifer' or d_fname = 'Jennifer') and initcap(o_lname) = 'Marsteen') or 
((initcap(o_fname) = 'Jennifer' or d_fname = 'Jennifer') and initcap(o_lname) = 'Navarro') or 
((initcap(o_fname) = 'Jenny' or d_fname = 'Jenny') and initcap(o_lname) = 'Bickford') or 
((initcap(o_fname) = 'Joseph' or d_fname = 'Joseph') and initcap(o_lname) = 'Krovoza') or 
((initcap(o_fname) = 'Judy' or d_fname = 'Judy') and initcap(o_lname) = 'Nagai') or 
((initcap(o_fname) = 'Karen' or d_fname = 'Karen') and initcap(o_lname) = 'Block') or 
((initcap(o_fname) = 'Karen' or d_fname = 'Karen') and initcap(o_lname) = 'Charney') or 
((initcap(o_fname) = 'Kathy' or d_fname = 'Kathy') and initcap(o_lname) = 'Barrientes') or 
((initcap(o_fname) = 'Kelly' or d_fname = 'Kelly') and initcap(o_lname) = 'Scott') or 
((initcap(o_fname) = 'Lana' or d_fname = 'Lana') and initcap(o_lname) = 'Wickliffe') or 
((initcap(o_fname) = 'Leigh' or d_fname = 'Leigh') and initcap(o_lname) = 'Hartman') or 
((initcap(o_fname) = 'Loretta' or d_fname = 'Loretta') and initcap(o_lname) = 'Pehanich') or 
((initcap(o_fname) = 'Martha' or d_fname = 'Martha') and initcap(o_lname) = 'Ozonoff') or 
((initcap(o_fname) = 'Melissa' or d_fname = 'Melissa') and initcap(o_lname) = 'Haworth') or 
((initcap(o_fname) = 'Michael' or d_fname = 'Michael') and initcap(o_lname) = 'Angius') or 
((initcap(o_fname) = 'Michael' or d_fname = 'Michael') and initcap(o_lname) = 'Tentis') or 
((initcap(o_fname) = 'Mona' or d_fname = 'Mona') and initcap(o_lname) = 'Ellerbrock') or 
((initcap(o_fname) = 'Nick' or d_fname = 'Nick') and initcap(o_lname) = 'Dolce') or 
((initcap(o_fname) = 'Nora' or d_fname = 'Nora') and initcap(o_lname) = 'Jimenez') or 
((initcap(o_fname) = 'Pamela' or d_fname = 'Pamela') and initcap(o_lname) = 'Pacelli') or 
((initcap(o_fname) = 'Patrick' or d_fname = 'Patrick') and initcap(o_lname) = 'Nolan') or 
((initcap(o_fname) = 'Paul' or d_fname = 'Paul') and initcap(o_lname) = 'Prokop') or 
((initcap(o_fname) = 'Robert' or d_fname = 'Robert') and initcap(o_lname) = 'August') or 
((initcap(o_fname) = 'Robert' or d_fname = 'Robert') and initcap(o_lname) = 'Kerr') or 
((initcap(o_fname) = 'Robert' or d_fname = 'Robert') and initcap(o_lname) = 'Ware') or 
((initcap(o_fname) = 'Sallie' or d_fname = 'Sallie') and initcap(o_lname) = 'Tate') or 
((initcap(o_fname) = 'Shari' or d_fname = 'Shari') and initcap(o_lname) = 'Kawelo') or 
((initcap(o_fname) = 'Shaun' or d_fname = 'Shaun') and initcap(o_lname) = 'Keister') or 
((initcap(o_fname) = 'Shelley' or d_fname = 'Shelley') and initcap(o_lname) = 'Maddex') or 
((initcap(o_fname) = 'Sumiko' or d_fname = 'Sumiko') and initcap(o_lname) = 'Hong') or 
((initcap(o_fname) = 'Sunny' or d_fname = 'Sunny') and initcap(o_lname) = 'Mason') or 
((initcap(o_fname) = 'Tami' or d_fname = 'Tami') and initcap(o_lname) = 'Oppedahl') or 
((initcap(o_fname) = 'Thomas' or d_fname = 'Thomas') and initcap(o_lname) = 'Mackey') or 
((initcap(o_fname) = 'Thomas' or d_fname = 'Thomas') and initcap(o_lname) = 'Venturino') or 
((initcap(o_fname) = 'Tony' or d_fname = 'Tony') and initcap(o_lname) = 'Hazarian') or 
((initcap(o_fname) = 'Virginia' or d_fname = 'Virginia') and initcap(o_lname) = 'Davis')

order by people.iamid asc, 
assc.percent_fulltime asc, 
assc.assoc_end_date asc;

spool off;