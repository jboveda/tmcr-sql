set echo off;
spool "/home/jboveda/Documents/Projects/TMCR/csvdrop-prod/tmcr.ucd_department.fromoracle.csv";

select /*csv*/
dept.DEPT_CODE as dept_code,
dept.dept_display_name as department_name,
college.dept_official_name as college_name,
'Department' as record_type

from pr_services.lov_pps_depts dept
left outer join pr_services.lov_pps_bous college
on dept.bou_orgoid = college.orgoid
order by dept.dept_code asc;

spool off;

