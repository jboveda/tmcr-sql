select 
replace(ltrim(rtrim(sponsors.spn)), ' ', '-') + '-' + cases.caseno as case_sponsor_id,
cases.caseno as case_id,
(case 
when 1=0 then 'UC Sponsor'
else 'Non-UC Sponsor'
end) as sponsor_role,
ltrim(rtrim(sponsors.spn)) as sponsor_account,
(case
when 1=0 then 'Non-Standard'
else 'Standard'
end) as sponsorship_terms,
(case sponsors.spn_typ
when 'GF' then 'Federal'
when 'GS' then 'State'
when 'GC' then 'County'
when 'GL' then 'Local/City'
when 'C'  then 'Commercial'
when 'O'  then 'Other'
when 'N'  then 'Non-profit'
end) as sponsor_type

from 
UCD_extract.dbo.case_spn cases
join UCD_extract.dbo.sponsor sponsors
on (sponsors.spn = cases.spn)

order by case_id asc

/*
(case cases.typ
when 'G' then 'Government'
when 'C' then 'Commercial'
when 'NP' then 'Non-profit'
when 'O' then 'Other'
end) as grant_type,
*/