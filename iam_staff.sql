set echo off
spool "/home/jboveda/Documents/Projects/TMCR/csvdrop/tmcr.iam_staff.fromoracle.csv"

select 
/*csv*/
people.iamid,
people.d_fname,
people.d_lname,
people.d_fullname,
people.is_faculty,
people.is_staff,

assc.dept_code,
assc.title_display_name,
assc.percent_fulltime,

contact.email,
contact.addr_street,
contact.addr_city,
contact.addr_state,
contact.addr_zip,
contact.postaladdress,
contact.work_phone,
contact.work_cell,
contact.work_pager,
contact.work_fax,

kerb.userid || '@ucdavis.edu' as federated_id

from pr_services.cv_people people 
left outer join pr_services.ppl_contact contact
on people.iamid = contact.iamid
inner join pr_services.ppl_pps_associations assc
on people.iamid = assc.iamid
inner join pr_services.ppl_pri_kerb_account kerb
on people.iamid = kerb.iamid

where 
assc.DEPT_CODE IN (
'061031',
'063000',
'061019',
'061021',
'061022',
'061025',
'061028',
'061029',
'061034',
'061037',
'061039',
'061000',
'061010',
'061020',
'061026',
'061200',
'061798',
'061800',
'061801',
'061811',
'061821',
'061843'
)

order by people.iamid asc, 
assc.percent_fulltime asc, 
assc.assoc_end_date asc;

spool off;