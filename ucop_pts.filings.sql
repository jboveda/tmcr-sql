declare @marketing table(keyid varchar(2), label varchar(100));
insert into @marketing values
('AA', 'Available'),
('AD', 'Active discussion'),
('AM', 'Active marketing'),
('AP', 'Available, passive'),
('IA', 'In abeyance'),
('II', 'Interinstitution, inst mkts'),
('IN', 'Initial'),
('LT', 'Litigation, do not market'),
('NE', 'Negotiation, exclusive'),
('NI', 'Negotiation, interinstitutional'),
('NL', 'Negotiation, limited'),
('NN', 'Negotiation, non-exclusive'),
('SP', 'Sponsor obligation'),
('XE', 'Optioned/Licensed, exclusive'),
('XL', 'Optioned/Licensed, limited'),
('XN', 'Optioned/Licensed, non-exclusive'),
('TA', 'Transferred with agreements'),
('TZ', 'Transferred'),
('ZZ', 'No potential'),
('U' , 'Uncertain');

select
-- US QUERY
cases.full_case + '-' + 'US' as filing_id,
cases.caseno as case_num,
cases.caseseq as case_seq,
'US' as cc,

cast(cases.publdt as date) as publication_date,
ltrim(rtrim(cases.publno)) as publication_number,
cast(cases.iss_dt as date) as patent_issue_date,
ltrim(rtrim(cases.disc_typ)) as filing_type,
marketing_status.label as marketing_status

from 
UCD_extract.dbo.case_m cases
left outer join @marketing marketing_status
on cases.mktg_stat = marketing_status.keyid

union all

-- FOREIGN QUERY
select
cases2.full_case + '-' + ltrim(rtrim(ff.ctry)) as filing_id,
cases2.caseno as case_num,
cases2.caseseq as case_seq,
ltrim(rtrim(ff.ctry)) as cc,

cast(cases2.publdt as date) as publication_date,
ltrim(rtrim(cases2.publno)) as publication_number,
cast(ff.issue_dt as date) as patent_issue_date,
ltrim(rtrim(cases2.disc_typ)) as filing_type,
marketing_status.label as marketing_status

from 
UCD_extract.dbo.case_m cases2
inner join UCD_extract.dbo.case_ff ff
on (cases2.caseno = ff.caseno and cases2.caseseq = ff.caseseq)
left outer join @marketing marketing_status
on cases2.mktg_stat = marketing_status.keyid

order by filing_id asc