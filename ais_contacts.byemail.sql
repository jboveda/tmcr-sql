set echo off;
spool "/home/jboveda/Documents/Projects/TMCR/csvdrop/tmcr.ais_contacts.fromoracle.csv";

select /*csv*/
people.iamid,
people.d_fname,
people.d_lname,
people.d_fullname,

assc.dept_code,
assc.title_display_name,
assc.percent_fulltime,

contact.email,
contact.addr_street,
contact.addr_city,
contact.addr_state,
contact.addr_zip,
contact.postaladdress,
contact.work_phone,
contact.work_cell,
contact.work_pager,
contact.work_fax

from pr_services.cv_people people
inner join pr_services.ppl_pps_associations assc
on people.iamid = assc.iamid
left outer join pr_services.ppl_contact contact
on people.iamid = contact.iamid

where contact.email in (
'cascott@ucdavis.edu', 
'christine.davis@ucdmc.ucdavis.edu', 
'cjcobbs@vetmed.ucdavis.edu', 
'cldavis@ucdavis.edu', 
'cmschmidt@ucdavis.edu', 
'dsisidor@ucdavis.edu', 
'dwdavis@ucdavis.edu', 
'hyemi.sevening@ucdmc.ucdavis.edu', 
'jacquelyn.kay-mills@ucdmc.ucdavis.edu', 
'jdkrovoza@ucdavis.edu', 
'jsberry@ucdavis.edu', 
'karen.scott@ucdmc.ucdavis.edu', 
'kdscott@ucdavis.edu', 
'klblock@ucdavis.edu', 
'klcharney@ucdavis.edu', 
'kmblock@ucdavis.edu', 
'ksbarrientes@ucdavis.edu', 
'lahartman@ucdavis.edu', 
'lwickliffe@ucdavis.edu', 
'mdhaworth@ucdavis.edu', 
'michaels.hong@ucdmc.ucdavis.edu', 
'mjozonoff@ucdavis.edu', 
'padavis@ucdavis.edu', 
'pjprokop@ucdavis.edu', 
'pmpacelli@ucdavis.edu', 
'rdavis@ucdavis.edu', 
'robert.august@ucdmc.ucdavis.edu', 
'salliegrace.tate@ucdmc.ucdavis.edu', 
'sbmaddex@ucdavis.edu', 
'skohong@ucdavis.edu', 
'tami.oppedahl@ucdmc.ucdavis.edu', 
'tcmackey@ucdavis.edu', 
'twscott@ucdavis.edu')

order by people.iamid asc, 
assc.percent_fulltime asc, 
assc.assoc_end_date asc;

spool off;