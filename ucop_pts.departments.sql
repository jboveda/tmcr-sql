declare @schools table(keyid varchar(5), label varchar(100));
insert into @schools values
('A&ES', 'College of Agricultural and Environmental Sciences'),
('DBS', 'College of Biological Sciences'),
('ENGR', 'College of Engineering'),
('GSM', 'Graduate School of Management'),
('L&S', 'College of Letters and Science'),
('LAW', 'School of Law'),
('MED', 'School of Medicine'),
('UCDMC', 'UCDMC - Hospitals and Clinics'),
('UNIX', 'College of Engineering'),
('VM', 'School of Veterinary Medicine');

declare @campus table(keyid varchar(3), label varchar(100));
insert into @campus values
('DA' , 'UC Davis'),
('BK' , 'UC Berkeley'),
('IR' , 'UC Irvine'),
('LA' , 'UCLA'),
('LAL', 'Los Alamos Laboratory'),
('LBL', 'Lawrence Berkeley Lab'),
('LLL', 'Lawrence Livermore Lab'),
('MC' , 'UC Merced'),
('NUC', 'Other non-university'),
('OP' , 'University of California Office of the President'),
('RV' , 'UC Riverside'),
('SB' , 'UC Santa Barbara'),
('SC' , 'UC Santa Cruz'),
('SD' , 'UC San Diego'),
('SF' , 'UC San Francisco'),
('GLA', 'Gladstone'),
('NGL', 'Non-gladstone');

select
ltrim(rtrim(fkey)) as fkey,
coalesce(labels.label, 'Public Service Research') as school,
'Department' as record_type,
'' as organization_type,
campus.keyid as parent_school,

(case
when school.s_camp != 'DA' then campus.label
when ltrim(rtrim(school.dept)) = '' then ltrim(rtrim(school.fullname))
else replace(ltrim(rtrim(school.schl)), 'DBS', 'CBS') + ' - ' + ltrim(rtrim(school.fullname))
end) as fullname

from 
UCD_extract.dbo.schldept school
join UCD_extract.dbo.campus camp
on (school.s_camp = camp.camp)
left outer join @schools labels
on labels.keyid = school.schl
join @campus campus
on campus.keyid = school.s_camp

where
school.schl != 'OTHER'
-- and school.s_camp != 'DA'
and school.s_camp = 'DA'

union all

select 
campus.keyid as fkey,
campus.label as school,
'Organization' as record_type,
'Academic or Research Institute' as organization_type,
'' as parent_school,
campus.label as fullname

from @campus campus

order by fkey asc
