set echo off
spool "/home/jboveda/Documents/Projects/TMCR/csvdrop/tmcr.user_accounts.fromoracle.csv"

select 
/*csv*/
people.iamid,
people.d_fname,
people.d_lname,
people.d_fullname,
people.is_faculty,
people.is_staff,

assc.dept_code,
assc.title_display_name,
assc.percent_fulltime,

contact.email,
contact.addr_street,
contact.addr_city,
contact.addr_state,
contact.addr_zip,
contact.postaladdress,
contact.work_phone,
contact.work_cell,
contact.work_pager,
contact.work_fax,

kerb.userid || '@ucdavis.edu' as federated_id

from pr_services.cv_people people 
left outer join pr_services.ppl_contact contact
on people.iamid = contact.iamid
inner join pr_services.ppl_pps_associations assc
on people.iamid = assc.iamid
inner join pr_services.ppl_pri_kerb_account kerb
on people.iamid = kerb.iamid

where 
contact.email in (
'abakshi@ucdavis.edu',
'baboczar@ucdavis.edu',
'najames@ucdavis.edu',
'drmcgee@ucdavis.edu',
'chneagley@ucdavis.edu',
'mthomsen@ucdavis.edu',
'jabonoan@ucdavis.edu',
'jdcarmikle@ucdavis.edu',
'mdcarriere@ucdavis.edu',
'chakhovs@ucdavis.edu',
'slfinney@ucdavis.edu',
'rgururajan@ucdavis.edu',
'dlmeade@ucdavis.edu',
'nerashid@ucdavis.edu',
'bnr393@ucdavis.edu',
'esisman@ucdavis.edu',
'kosmith@ucdavis.edu',
'rlstears@ucdavis.edu',
'ssyphers@ucdavis.edu',
'srthompson@ucdavis.edu',
'mlellerbrock@ucdavis.edu',
'kmfarrand@ucdavis.edu',
'jfjones@ucdavis.edu',
'haroldsen@ucdavis.edu',
'jboveda@tmcr.ucdavis.edu',
'ucdavis@acfsolutions.com',
'jkshattuck@ucdavis.edu',
'api@tmcr.ucdavis.edu',
'gdurante@ucdavis.edu',
'slockett@ucdavis.edu',
'zstarke@ucdavis.edu',
'jfjones@ucdavis.edu',
'dpathak@ucdavis.edu',
'bnroberts@ucdavis.edu'
)

order by people.iamid asc, 
assc.percent_fulltime asc, 
assc.assoc_end_date asc;

spool off;