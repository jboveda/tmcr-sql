select
replace(ltrim(rtrim(people.nk)), ' ', '-') as pts_name_id,

--name
ltrim(rtrim(people.prefix)) as salutation,
replace(ltrim(rtrim(people.fname)), '"', '') as first_name,
substring(ltrim(rtrim(people.minit)), 1, 1) as middle_initial,
ltrim(rtrim(people.lname)) as last_name,

--organization
ltrim(rtrim((case 
when school.s_camp != 'DA' then school.s_camp
when school.dept is null and school.schl is null then 'DA'
else school.fkey end))) as department_id,
--lower(school.dept) as department,

--contact info
ltrim(rtrim(ISNULL(people.email_wrk, people.email_hom))) as email,
ltrim(rtrim(people.m_add0 + ISNULL(people.m_add1, ''))) as street,
ltrim(rtrim(people.m_add2)) as city,
ltrim(rtrim(people.m_st)) as state,
ltrim(rtrim(people.m_zip)) as zip,
ltrim(rtrim(people.m_ctry)) as country,
ltrim(rtrim(people.m_tel)) as phone,
ltrim(rtrim(people.m_fax)) as fax

from 
UCD_extract.dbo.name_ad people
join (select distinct(inv_nk) as inv_nk from UCD_extract.dbo.case_inv) inventor
on (inventor.inv_nk = people.nk)
left outer join UCD_extract.dbo.schldept school
on (school.schl = people.schl
and school.dept = people.dept
and school.s_camp = people.n_camp)

where 
people.lname not in ('Unassigned', '0', '')

order by people.nk asc